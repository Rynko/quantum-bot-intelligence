from asyncio.windows_events import NULL
from pickle import NONE
import queue
import random
import shutil
import yt_dlp
import discord
import os
import pyttsx3
import asyncio
import keepalive_pkg
import pytdm
from discord.ext import commands

folder = 'music_files'

yt_dlp.utils.bug_reports_message = lambda: ''
ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': 'music_files/%(id)s.mp3',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
    
}

ffmpeg_options = {
    'options': '-vn',
    "before_options": "-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5"
}

ytdl = yt_dlp.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_running_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

client = commands.Bot(command_prefix='')
engine = pyttsx3.init()

@client.command(name='barka')
async def barka(ctx):
    await ctx.send("https://www.youtube.com/watch?v=CGLLglu9jaI")


@client.command()
async def wąż(ctx):
    await client.change_presence(activity=discord.Game("Sługa węża"))





@client.command()
async def penis(ctx, arg):
    x=int(arg)
    eq = "="
    if x<100 and x>0:
        await ctx.channel.send("B" + x*eq +"D")


@client.command()
async def sanah(ctx):
    songs = ["https://youtu.be/43Gs5Jvfx4w",
             "https://youtu.be/hHb3owr6PQg",
             "https://youtu.be/uGWo8WwletU",
             "https://youtu.be/YCN67r3TBQk",
             "https://youtu.be/bpVT2vRSsSU",
             "https://youtu.be/pxikqLzKZDY",
             "https://youtu.be/es1GWAgfGck",
             "https://youtu.be/dldxQCRSG4U",
             "https://youtu.be/S7sN-cFhwuk",
             "https://youtu.be/TLlOTniJgNU",
             "https://youtu.be/AkBvAOWZbwc",
             "https://youtu.be/FKee5VCDVxg",
             "https://youtu.be/UNujEZdW9GI",
             "https://youtu.be/ViVIOQdzYno",
             "https://youtu.be/uJztp6ezeVI",
             "https://youtu.be/9RZ-QDXym0k",
             "https://youtu.be/z1q9NJ1Ur6M",
             "https://youtu.be/tk6HKgiaQ30",
             "https://youtu.be/c8WDzsqQ7UY",
             "https://youtu.be/2A62oBDlvBQ"]
    await ctx.channel.send(random.choice(songs))


@client.event
async def on_ready():
    print("Working...")
    await client.change_presence(activity=discord.Game(name="Sługa węża"))


queue=[]
async def playSong(ctx, channel):
    async with ctx.typing():
        voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
        if len(queue)>0:
            song = queue[0]
            if voice.is_playing():
                await ctx.send("Added to queue")
            player = await YTDLSource.from_url(song, loop=client.loop, stream=True)
            channel.play(player)
            await ctx.send('**Now playing:** {}'.format(player.title))
            queue.pop(0)
    while song!= NULL:
        while voice.is_playing() and not voice.is_paused():
            await asyncio.sleep(1)
        await playSong(ctx, channel)    
    await ctx.send('**Now playing:** {}'.format(player.title))



@client.command(name='play', help='This command plays music')
async def play(ctx, *args):
    title = args[:]
    url = " ".join(title)
    if not ctx.message.author.voice:
        await ctx.send("You are not connected to a voice channel")
        return

    else:
        channel = ctx.message.author.voice.channel
    
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if channel != voice and voice is None:
        await channel.connect()
    queue.append(url)
    print (queue)
    server = ctx.message.guild
    voice_channel = server.voice_client
    await playSong(ctx, voice_channel)
    async with ctx.typing():
        player = await YTDLSource.from_url(url, loop=client.loop, stream=True)
        voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)

    
@client.command()
async def skip(ctx):
    server = ctx.message.guild
    voice_channel = server.voice_client
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice.is_playing() and len(queue)>0:
        voice.stop()
        await playSong(ctx, voice_channel)
        print("here")
    else:
        await ctx.send("Ludzie przecież tam niczego nie ma")


@client.command()
async def pause(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice.is_playing():
        voice.pause()
        await ctx.send("Paused")
    else:
        await ctx.send("Currently no audio is playing.")

@client.command()
async def resume(ctx):
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice.is_paused():
        voice.resume()
    else:
        await ctx.send("The audio is not paused.")

@client.command(name='stop', help='This command stops the music and makes the bot leave the voice channel')
async def stop(ctx):
    queue.clear()
    voice_client = ctx.message.guild.voice_client
    await voice_client.stop()

@client.command()
async def leave(ctx):
    for song in os.listdir(folder):
                file_path = os.path.join(folder, song)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print('Failed to delete %s. Reason %s' % (file_path, e))
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if voice.is_connected():
        await voice.disconnect()
    else:
        await ctx.send("The bot is not connected to a voice channel.")

@client.command()
async def tts(ctx, *args):
    test_there = os.path.isfile("test.mp3")
    try:
        if test_there:
            os.remove("test.mp3")
    except PermissionError:
        await ctx.send("Chuj ci w dupe, nie można")
        return 
    engine.save_to_file(" ".join(args[:]).replace('’','').replace('"','') , 'test.mp3')
    engine.runAndWait()
    voiceChannel = ctx.author.voice.channel
    print(voiceChannel)
    await voiceChannel.connect()
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    try:
        if not voice.is_playing():
            print()
    except:
        await ctx.send("Chuj ci w dupe, nie można")
        return
    voice.play(discord.FFmpegPCMAudio("test.mp3"))
    while voice.is_playing():
        await asyncio.sleep(1)
    await voice.disconnect()


@client.command()
async def tdm(ctx, *args):
    test_there = os.path.isfile("test.mp3")
    try:
        if test_there:
            os.remove("test.mp3")
    except PermissionError:
        await ctx.send("Chuj ci w dupe, nie można")
        return 
    pytdm.zapisz(" ".join(args[:]).replace('’','').replace('"','') , 'test.mp3')
    voiceChannel = ctx.author.voice.channel
    print(voiceChannel)
    await voiceChannel.connect()
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    try:
        if not voice.is_playing():
            print()
    except:
        await ctx.send("Chuj ci w dupe, nie można")
        return
    voice.play(discord.FFmpegPCMAudio("test.mp3"))
    while voice.is_playing():
        await asyncio.sleep(1)
    await voice.disconnect()

@client.command()
async def twoja(ctx, arg):
    twoja_stara=["zapierdala do biedronki po kondomki", "robi zdjęcia aparatem słuchowym",
                 "ogląda pornosy do końca, bo myśli, że będzie ślub", "ciągnie rzepę w familiadzie",
                 "odpowiada za wpływ zorzy polarnej na rozmnażanie się pingwinów w gołoledzi we fiordach norweskich",
                 "jest taka gruba, że jak chodziła do szkoły, to siedziała obok wszystkich", "jebanana",
                 "zapierdala, a Twój stary ją pogania", "jest tak stara, że wisi 5 zł Mojżeszowi",
                 "sieje grubymi nićmi", "prowadzi pociąg w Teleexpressie", "wychodzi w nocy na dach i tańczy z gwiazdami",
                 "wyjada żelatynę z galaretki", "woła o pomstę do nieba", "weszła dwa razy do tej samej rzeki",
                 "siedzi za samobójstwo", "poważnie szkodzi sobie i osobom w jej otoczeniu", "kozłuje piłką lekarską na plaży",
                 "przeszła Need for Speed na piechotę", "to ulica a Twój stary to Kubica",
                 "wlazła na płotek i mruga", "pamięta jak Burger King był jeszcze księciem",
                 "umieszcza ogłoszenia na Tablicy Mendelejewa", ]
    a = str(arg).lower()
    if a == "stara":
        tmp = "twoja stara " + random.choice(twoja_stara)
        await ctx.send(tmp)
    test_there = os.path.isfile("test.mp3")
    try:
        if test_there:
            os.remove("test.mp3")
    except PermissionError:
        await ctx.send("Chuj ci w dupe, nie można")
        return 

    pytdm.zapisz(tmp , 'test.mp3')
    voiceChannel = ctx.author.voice.channel
    print(voiceChannel)
    await voiceChannel.connect()
    voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
    try:
        if not voice.is_playing():
            print()
    except:
        await ctx.send("Chuj ci w dupe, nie można")
        return
    voice.play(discord.FFmpegPCMAudio("test.mp3"))
    while voice.is_playing():
        await asyncio.sleep(1)
    await voice.disconnect()


client.run("OTU4ODEzOTUzNzYzNDU1MDE3.YkSzFA.8XHV8kd-NVUgE0U_b9kC_dJezlw")


